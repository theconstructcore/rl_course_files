#!/usr/bin/env python
# Author: Markus Buchholz 

import rospy
from geometry_msgs.msg import Twist, Point, Quaternion
import tf
from math import radians, copysign, sqrt, pow, pi, atan2
from tf.transformations import euler_from_quaternion
import numpy as np

#========================================================

 
#org 
graph = np.array([  [0,0,0,0,0,0,0,0,0,0],  # J
                    [0,0,0,0,0,0,0,0,0,4],  # I
                    [0,0,0,0,0,0,0,0,0,3],  # H
                    [0,0,0,0,0,0,0,3,3,0],  # G
                    [0,0,0,0,0,0,0,6,3,0],  # F
                    [0,0,0,0,0,0,0,1,4,0],  # E
                    [0,0,0,0,4,1,5,0,0,0],  # D
                    [0,0,0,0,3,2,4,0,0,0],  # C
                    [0,0,0,0,7,4,6,0,0,0],  # B
                    [0,2,4,3,0,0,0,0,0,0]]) # A

# if you want tast other graphs (other path the robot chooses so please change th name graphXX to graph)
# you are welcome to prepare your own graph

graphXX = np.array([  [0,0,0,0,0,0,0,0,0,0],  # J
                    [0,0,0,0,0,0,0,0,0,1],  # I
                    [0,0,0,0,0,0,0,0,0,3],  # H
                    [0,0,0,0,0,0,0,3,1,0],  # G
                    [0,0,0,0,0,0,0,6,3,0],  # F
                    [0,0,0,0,0,0,0,1,4,0],  # E
                    [0,0,0,0,4,1,1,0,0,0],  # D
                    [0,0,0,0,3,2,4,0,0,0],  # C
                    [0,0,0,0,7,4,6,0,0,0],  # B
                    [0,2,4,1,0,0,0,0,0,0]]) # A

#Name of nodes (waypoins)
node_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

#########################################################################
## PART ONE - DP COMPUTATION
#########################################################################
"""
Run the same code as in Jupyter - just compute optimal path using DP
"""

def compute (graph):
    mem = np.zeros(10)
    paths = []
    for i in range (len(graph)):
        temp = []
        temp_pos = []
        
        for j in range(len(graph)):
            if graph[i][j] > 0:
                temp.append(graph[i][j] + mem[len(graph) - 1 -j])
                temp_pos.append(j)
        if len(temp)>0:
            mem[i] = min(temp)   
            paths.append(temp_pos) 
    mem[::-1], paths[::-1] = mem, paths
    return mem, paths

def optimal_node(mem,paths, node):

    value_nodes = []
    for path in paths:
        temp_v_node = []
        for j in path:
            temp_v_node.append(mem[j])
        value_nodes.append(temp_v_node)

    next_node_value = value_nodes[node].index(min(value_nodes[node]))
    next_node_index = paths[node][next_node_value]

    return next_node_index

def optimal_path_draw(mem, path):
    
    optimal_path = []
    optimal_path.append(0)
    node = optimal_node(mem,path, 0)
    while(node < 9):
        optimal_path.append(node)
        node = optimal_node(mem,path, node)
    optimal_path.append(9)

    return optimal_path

"""
functioin to check optimal path and compute the nodes (waypoints) for the robot
"""
def compute_waypoints(g):

    graph = g
    temp_arr = []
    waypoints = []
    mem, paths = compute(graph)
    optimal_path = optimal_path_draw(mem, paths)
    

    for i in optimal_path:
       
        temp_arr.append(node_names[i])
    
    arr = [temp_arr[i] + temp_arr[i+1] for i in range(len(temp_arr)-1)]
    #print(arr)
    #print(temp_arr)
    way = 2
    for i in arr: #range(len(arr)):
        if ((i == 'AB') or (i == 'CE') or (i == 'DF')):
            ang = 0.78
        if ((i == 'AD') or (i == 'CG') or (i == 'BF')):
            ang = -0.78
        if ((i == 'BG')):
            ang = -1.11
        if ((i == 'DE')):
            ang = 1.11
        if ((i == 'FH') or (i == 'GI') or (i == 'IJ')):
            ang = 0.4
        if ((i == 'EH') or (i == 'HJ') or (i == 'FI')):
            ang = -0.4
        if ((i == 'GH')):
            ang = 0.85
        if ((i == 'EI')):
            ang = -0.85
        if ((i == 'AC') or (i == 'CF') or (i == 'BE')):
            ang = -0.001
        if ((i == 'DG')):
            ang = 0.001
        
        waypoints.append([ang,way])
        way +=2
# functiion return array with waypoint which will be passed by robot
    
    return waypoints

#########################################################################
## PART TWO - MOTION
#########################################################################
"""
Definiot of motion class which takes a waypoint(node) one by one and move the robot accordingly
"""

class GotoPoint():
    

    def __init__(self, pos_x, pos_y, pos_ang, flag):
        rospy.init_node('turtlebot3_pointop_key', anonymous=False)
        rospy.on_shutdown(self.shutdown)
        self.cmd_vel = rospy.Publisher('cmd_vel', Twist, queue_size=5)
        position = Point()
        move_cmd = Twist()
        r = rospy.Rate(10)
        self.tf_listener = tf.TransformListener()
        self.odom_frame = 'odom'
        self.go_Flag = flag
        self.goal_x = pos_x
        self.goal_y = pos_y
        self.goal_ang = pos_ang
        


        try:
            self.tf_listener.waitForTransform(self.odom_frame, 'base_footprint', rospy.Time(), rospy.Duration(1.0))
            self.base_frame = 'base_footprint'
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            try:
                self.tf_listener.waitForTransform(self.odom_frame, 'base_link', rospy.Time(), rospy.Duration(1.0))
                self.base_frame = 'base_link'
            except (tf.Exception, tf.ConnectivityException, tf.LookupException):
                rospy.loginfo("Cannot find transform between odom and base_link or base_footprint")
                rospy.signal_shutdown("tf Exception")


        position, rotation = self.get_odom()
       
        linear_speed = 1
        angular_speed = 1


        y_start = 0
        x_start = 0
        goal_z = self.goal_ang 

        if self.go_Flag == True:
        # first the robot rotate to the given angle
            if goal_z > 0: 
                while abs(goal_z - rotation) > 0.05:
                    position, rotation = self.get_odom()
                    rospy.loginfo("rotation")
                    print(rotation)
                    rospy.loginfo("goal_z")
                    print(goal_z)
                    rospy.loginfo("diff")
                    print(abs(rotation - goal_z))
                    move_cmd.linear.x = 0.00
                    move_cmd.angular.z = 0.1

                    self.cmd_vel.publish(move_cmd)
                    r.sleep()

            if goal_z < 0: 
                while abs(goal_z - rotation) > 0.05:
                    position, rotation = self.get_odom()
                    rospy.loginfo("rotation")
                    print(rotation)
                    rospy.loginfo("goal_z")
                    print(goal_z)
                    rospy.loginfo("diff")
                    print(abs(rotation - goal_z))
                    move_cmd.linear.x = 0.0
                    move_cmd.angular.z = -0.1

                    self.cmd_vel.publish(move_cmd)
                    r.sleep()
            
            print("###################################################")
            position, rotation = self.get_odom()
            goal_distance = sqrt(pow(self.goal_x - position.x, 2) + pow(self.goal_y - position.y, 2))
            goal_distance_mod_x = self.goal_x - position.x
            #goal_distance_mod_y = self.goal_y - position.y
            print(goal_distance_mod_x) 
            print("###################################################")

            # robot goes to define node

            while (goal_distance_mod_x > 0.05):
                position, rotation = self.get_odom()
                goal_distance_mod_x = self.goal_x - position.x
                print("------GOAL------------------")
                print(goal_distance_mod_x) 
                print("------POSITION------------------")
                print(position.x, " ::",  rotation)
                move_cmd.linear.x = 0.2
                move_cmd.angular.z = 0.0
                self.cmd_vel.publish(move_cmd)
                r.sleep()
                if goal_distance_mod_x <0.05:
                    move_cmd.linear.x = 0.0
                    move_cmd.angular.z = 0.0

                    self.go_Flag = False
                   

        print("********BYE BYE !!!*******")



    def get_odom(self):
        #rospy.loginfo("### GET ODOM ###")
        try:
            (trans, rot) = self.tf_listener.lookupTransform(self.odom_frame, self.base_frame, rospy.Time(0))
            rotation = euler_from_quaternion(rot)

        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo("TF Exception")
            return

        return (Point(*trans), rotation[2])


    def shutdown(self):
        rospy.loginfo("### SHUTDOWN ###")
        self.cmd_vel.publish(Twist())
        rospy.sleep(1)

"""
Main function to run robot throught all computed nodes (waypoints)
Program stops after the robot reaches the goal
"""


if __name__ == '__main__':
    print("Hallo")
    waypoints = compute_waypoints(graph)
    for w in waypoints:
        print("===================new_goal============================")
        GotoPoint(w[1],2,w[0], True)





